package etcd

import "errors"

var (
	ErrConfigNotFound = errors.New("config not found")
)
