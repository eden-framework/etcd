package etcd

import (
	"bytes"
	"fmt"
	"gitee.com/eden-framework/envconfig"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"os"
)

func unmarshalFromConfig(content, prefix string, structureConfigs map[string]any, conf ...interface{}) error {
	decoder := yaml.NewDecoder(bytes.NewReader([]byte(content)))

	configs := make(map[string]string)
	err := decoder.Decode(&configs)
	if err != nil {
		return err
	}

	if structureConfigs != nil {
		for _, structureConf := range structureConfigs {
			err := decoder.Decode(structureConf)
			if err != nil {
				logrus.Errorf("unmarshal etcd config error: %v", err)
			}
		}
	}

	for key, value := range configs {
		fmt.Printf("export %s=%s\n", key, value)
		os.Setenv(key, value)
	}
	for _, c := range conf {
		err := envconfig.Process(prefix, c)
		if err != nil {
			return err
		}
		_ = envconfig.Usage(prefix, c, true)
	}
	return nil
}
