package etcd

import "gitee.com/eden-framework/envconfig"

type BaseConfig struct {
	EtcdConfigEnable         bool
	EtcdConfigKey            string
	EtcdEndpoints            []string
	EtcdUsername             string
	EtcdPassword             string
	EtcdDialTimeout          envconfig.Duration
	EtcdDialKeepAliveTime    envconfig.Duration
	EtcdDialKeepAliveTimeout envconfig.Duration
	EtcdMaxCallSendMsgSize   int
	EtcdMaxCallRecvMsgSize   int
}
