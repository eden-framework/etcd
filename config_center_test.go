package etcd

import (
	"fmt"
	"testing"
)

func TestAssignConfWithDefault(t *testing.T) {
	type args struct {
		config *BaseConfig
		prefix string
		conf   []any
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "",
			args: args{
				config: &BaseConfig{
					EtcdConfigKey:            "test-config",
					EtcdEndpoints:            []string{"webserver:30644"},
					EtcdUsername:             "",
					EtcdPassword:             "",
					EtcdDialTimeout:          0,
					EtcdDialKeepAliveTime:    0,
					EtcdDialKeepAliveTimeout: 0,
					EtcdMaxCallSendMsgSize:   0,
					EtcdMaxCallRecvMsgSize:   0,
				},
				prefix: "test",
				conf: []any{
					&struct {
						Host string `json:"host"`
						Port int    `json:"port"`
					}{},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				if err := AssignConfWithDefault(
					tt.args.config,
					tt.args.prefix,
					tt.args.conf...,
				); (err != nil) != tt.wantErr {
					t.Errorf("AssignConfWithDefault() error = %v, wantErr %v", err, tt.wantErr)
				}
				fmt.Printf("%+v", tt.args.conf[0])
			},
		)
	}
}
