package etcd

import (
	"context"
	"github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"time"
)

type Refresher interface {
	Refresh()
}

func AssignConfWithDefault(
	config *BaseConfig,
	prefix string,
	structureConfigs map[string]any,
	conf ...any,
) (err error) {
	if !config.EtcdConfigEnable {
		return
	}
	client, err := clientv3.New(
		clientv3.Config{
			Endpoints:            config.EtcdEndpoints,
			DialTimeout:          time.Duration(config.EtcdDialTimeout),
			DialKeepAliveTime:    time.Duration(config.EtcdDialKeepAliveTime),
			DialKeepAliveTimeout: time.Duration(config.EtcdDialKeepAliveTimeout),
			MaxCallSendMsgSize:   config.EtcdMaxCallSendMsgSize,
			MaxCallRecvMsgSize:   config.EtcdMaxCallRecvMsgSize,
			Username:             config.EtcdUsername,
			Password:             config.EtcdPassword,
		},
	)
	if err != nil {
		logrus.Errorf("new etcd client error: %v", err)
		return
	}

	content, err := client.Get(context.TODO(), config.EtcdConfigKey)
	if err != nil {
		logrus.Errorf("get etcd config error: %v", err)
		return
	}
	if len(content.Kvs) == 0 {
		logrus.Errorf("etcd config key %s not found", config.EtcdConfigKey)
		return ErrConfigNotFound
	}
	err = unmarshalFromConfig(string(content.Kvs[0].Value), prefix, structureConfigs, conf...)
	if err != nil {
		logrus.Errorf("unmarshal etcd config error: %v", err)
		return err
	}
	return
}
